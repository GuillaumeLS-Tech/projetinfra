import { React, useState } from "react";
import List from "./List.js"
import LogoAirbnb from "../airbnb-logo.png";

function Header(){

    const [inputText, setInputText] = useState("");
    let inputHandler = (e) => {
        //convert input text to lower case
        var lowerCase = e.target.value.toLowerCase();
        setInputText(lowerCase);
    };

    return (
        <div className="App">
            <head>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
            </head>
            <header className="App-header">
                <div className="nav">
                    <img src={LogoAirbnb} alt="Logo" className='LogoAirbnb'/>
                    <div className='searchBox'>
                    <input className="searchInput" type="text" placeholder="Rechercher" onChange={inputHandler}></input>
                    <button className="searchButton" href="#">
                    <i class="fa fa-search"></i>
                    </button>
                    </div>
                </div>
                <div className="faq">
                    <a href="#">FAQ</a>
                </div>
            </header>
            <div>
                <List input={inputText}/>
            </div>
        </div>
    );
}

export default Header;