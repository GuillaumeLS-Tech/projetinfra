import { React, useState } from 'react'
import data from "./ListData.json"
import '../App.css';

function List(props) {
    //create a new array by filtering the original array
    const filteredData = data.filter((el) => {
        //if no input the return the original
        if (props.input === '') {
            return el;
        }
        //return the item which contains the user input
        else {
            return el.text.toLowerCase().includes(props.input)
        }
    })
    return (
        <div className='destinationHome'>
            {filteredData.map((item) => (
                <>
                    <li className='destination' key={item.id}>
                        {item.text}{item.desc}<br/>
                        <img className='imgPays' src={item.img} alt="lorem"/>
                    </li>
                </>
            ))}
        </div>
    )
}

export default List