import { React, useState } from "react";
import './App.css';
import LogoAirbnb from "./airbnb-logo.png";
import List from "./Components/List.js";
import Header from './Components/Header.js';

function App() {

  const [inputText, setInputText] = useState("");
  let inputHandler = (e) => {
    //convert input text to lower case
    var lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };

  return (
    <div className="App">
      <Header />
      <div>
        
      </div>
    </div>
  );
}

export default App;
